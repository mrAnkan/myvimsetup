﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections;
using System.IO.Compression;

namespace MyVimSetup
{
    class Program
    {
	// Create an instance of http client.
        private static HttpClient http = new HttpClient();
        
	// Current directory where vim configuration is stored.
	private static string vimDirectory = "/home/daniel/vim";
       	
	// Specific tuple for pathogen to make sure it is installed.
       	private static Tuple<string, string> pathogen = Tuple.Create("tpope", "vim-pathogen");
        
	// Array of all plugins that arethat are  to be installed.
	private static Tuple<string, string>[] vimPlugins = 
        {
            Tuple.Create("Valloric", "YouCompleteMe"),
	    Tuple.Create("scrooloose", "nerdtree"),
            Tuple.Create("ryanoasis", "vim-devicons"),
            Tuple.Create("tiagofumo", "vim-nerdtree-syntax-highlight"),
            Tuple.Create("joshdick", "onedark.vim"),
            Tuple.Create("vim-airline", "vim-airline"),
            Tuple.Create("vim-syntastic", "syntastic"),
            Tuple.Create("tpope", "vim-dispatch"),
            Tuple.Create("OmniSharp", "omnisharp-vim"),
            Tuple.Create("tpope", "vim-sleuth"),
            Tuple.Create("OrangeT", "vim-csharp"),
            Tuple.Create("jiangmiao", "auto-pairs")
        };

	/**
	 * Installs provided plugins.
	 */
        static void Main(string[] args)
        {
	    if(args.Length > 0)
		vimDirectory = args[0]; 
            performVimSetup();
        }

	/**
	 * Method for starting the setup process.
	 */
	public static void performVimSetup() 
	{
	    // Create needed folders.
	    createFolder(vimDirectory, "autoload");
	    createFolder(vimDirectory, "bundle");
	    
	    // Install pathogen first.
	    installPathogen().Wait();
	    // Then install bulk of plugins.
	    bulkInstall();
	    // Lastly, copy the vimrc file.
	    copySettings();
	}
	
	/**
	 * Pathogen must be installed to run any plugin, this method ensures it is installed.
	 */
        private static async Task installPathogen() 
        {
          
	  HttpContent content = await downloadFromGithub(pathogen);

	  // Pathogen is to be placed in the folder named autoload and not the folder named bundle.
	  using(Stream zip = await content.ReadAsStreamAsync()) {
	     Console.WriteLine("Unzipping and Writing <tpope:pathogen>.");
	     var archive = new ZipArchive(zip);
	     archive.GetEntry("vim-pathogen-master/autoload/pathogen.vim").ExtractToFile(vimDirectory + "/autoload/pathogen.vim");
	  }
        }

	/**
	 * Install all plugins from provided list.
	 */
        private static void bulkInstall() 
        {
            // List to hold references to ongoing tasks.
	    ArrayList tasks = new ArrayList();

	    // Start concurrent download of plugins.
            foreach(Tuple<string, string> plugin in vimPlugins) 
            {
                tasks.Add(storePlugin(plugin));           
            }

	    // Collect and wait for all downloads.
            foreach(Task task in tasks) 
            {
                task.Wait();
            }
        }

	/**
	 * Get the plugin from GitHub and store on current machine.
	 */
        private static async Task storePlugin(Tuple<string, string> current) 
        {
            // Download zip with current version of plugin from github.
	    HttpContent content = await downloadFromGithub(current);
	    
	    // Wait for stream before usage and close when finished.
	    using(Stream zip = await content.ReadAsStreamAsync()) {
		// Write plugin to storage.
                writeToFile(current, zip);
	    }
        }

        /**
	 * Using a tuple with account and repository on GitHub, download zip file with files from current version of master.
	 */
	private static async Task<HttpContent> downloadFromGithub(Tuple<string, string> current)
        {
            Console.WriteLine($"Downloading <{current.Item1}:{current.Item2}>.");

            HttpResponseMessage response = await http.GetAsync(
                "https://github.com/" + current.Item1 + "/" + current.Item2 + "/archive/master.zip"
            );

            return response.Content;
        }

	/**
	 * Unpack and write a downloaded zip from github to a directory. The branch-name is removed from name of directory.  
	 */
        private static void writeToFile(Tuple<string, string> current, Stream zip) 
        { 
            Console.WriteLine($"Unzipping and Writing <{current.Item1}:{current.Item2}>.");
	    
	    // Unzip the stream to a directory·
	    unzipFile(zip, vimDirectory + "/bundle");
		
	    // Rename directory by removing the substring "-master"
	    Directory.Move(vimDirectory + "/bundle/" + current.Item2 + "-master/", vimDirectory + "/bundle/" + current.Item2); 
        }

	/**
	 * Unzips a stream to a directory.
	 */
        private static void unzipFile(Stream zip, string directory) 
        {
	    // Save zip content to new directory or override current.
	    var archive = new ZipArchive(zip);
	    archive.ExtractToDirectory(directory, true);
        }

	/**
	 * Creates a folder at given location with given name.
	 */
        private static void createFolder(string location, string name) 
        {
            Directory.CreateDirectory(location + "/" + name); 
        }

	/**
	 * Copies settings from pre-configured vimrc to current machine.
	 */
        private static void copySettings() 
        {
	    Console.WriteLine("Replacing current vimrc file with a pre-configured version.");
	    // Copy the entire pre-configured vimrc file and replace the vimrc at provided vim-dir.
	    File.Copy("vimrc", vimDirectory, true); 
        }
    }
}
